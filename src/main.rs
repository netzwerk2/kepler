use bevy::prelude::*;

use kepler::KeplerPlugin;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugins(KeplerPlugin)
        .run();
}
