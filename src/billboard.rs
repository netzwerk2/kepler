use crate::bodies::BodyPosition;
use bevy::core_pipeline::clear_color::ClearColorConfig;
use bevy::prelude::*;

use crate::camera::MainCamera;

pub struct BillboardPlugin;

impl Plugin for BillboardPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, setup)
            .add_systems(Update, update_billboard_positions);
    }
}

#[derive(Component, Deref, DerefMut)]
pub struct Billboard(pub Entity);

fn setup(mut commands: Commands) {
    commands.spawn(Camera2dBundle {
        camera: Camera {
            order: 1,
            ..default()
        },
        camera_2d: Camera2d {
            clear_color: ClearColorConfig::None,
        },
        ..default()
    });
}

fn update_billboard_positions(
    mut billboard_query: Query<(&Billboard, &mut Transform), Without<BodyPosition>>,
    body_query: Query<&BodyPosition>,
    camera_query: Query<(&Camera, &GlobalTransform), With<MainCamera>>,
) {
    for (billboard, mut billboard_transform) in billboard_query.iter_mut() {
        let position = body_query.get(**billboard).unwrap();
        let (camera, camera_transform) = camera_query.single();

        if let Some(viewport_pos) = camera.world_to_viewport(camera_transform, **position) {
            let logical_viewport_size = camera.logical_viewport_size().unwrap();
            billboard_transform.translation = (viewport_pos - logical_viewport_size * 0.5)
                .extend(0.0)
                * Vec3::new(1.0, -1.0, 1.0);
        }
    }
}
