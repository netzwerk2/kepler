use std::f64::consts::TAU;

use bevy::prelude::*;
use bevy_egui::egui::Grid;
use bevy_egui::egui::{DragValue, Ui, Window as EguiWindow};
use bevy_egui::EguiContexts;
use orbit_tessellation::bevy_polyline::prelude::*;
use orbit_tessellation::{Orbit, OrbitBundle};

use crate::bodies::BodyName;
use crate::solar_system::SolarSystemBody;
use crate::SUN_MU;

pub struct OrbitUiPlugin;

impl Plugin for OrbitUiPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<NewOrbit>()
            .add_systems(Update, orbit_settings_ui);
    }
}

// TODO: Manage time correctly
#[derive(Deref, DerefMut, Resource)]
pub struct NewOrbit(pub Orbit);

impl Default for NewOrbit {
    fn default() -> Self {
        Self(Orbit::new(1e11, 0.0, 0.0, 0.0, 0.0, 0.0, SUN_MU, 0.0))
    }
}

fn orbit_settings_ui(
    mut commands: Commands,
    mut egui_context: EguiContexts,
    mut orbit_param_set: ParamSet<(
        Query<(&mut Orbit, &BodyName)>,
        Query<&Orbit, Without<SolarSystemBody>>,
    )>,
    new_orbit: ResMut<NewOrbit>,
    mut polylines: ResMut<Assets<Polyline>>,
    mut materials: ResMut<Assets<PolylineMaterial>>,
) {
    EguiWindow::new("Orbit Settings")
        .fixed_pos((10.0, 10.0))
        .resizable(false)
        .show(egui_context.ctx_mut(), |ui| {
            // TODO: Sort bodies
            for (mut orbit, name) in orbit_param_set.p0().iter_mut() {
                ui.collapsing((**name).to_string(), |ui| {
                    let orbit_ref = &mut *orbit;

                    orbit_grid(
                        format!("{}_settings_grid", **name),
                        &mut orbit_ref.semi_major_axis,
                        &mut orbit_ref.eccentricity,
                        &mut orbit_ref.inclination,
                        &mut orbit_ref.longitude_of_ascending_node,
                        &mut orbit_ref.argument_of_periapsis,
                        &mut orbit_ref.mean_anomaly,
                        ui,
                    );
                });
            }

            ui.separator();

            create_orbit_ui(
                ui,
                &mut commands,
                new_orbit,
                &mut polylines,
                &mut materials,
                orbit_param_set.p1().iter().len(),
            );
        });
}

fn create_orbit_ui(
    ui: &mut Ui,
    commands: &mut Commands,
    mut new_orbit: ResMut<NewOrbit>,
    polylines: &mut Assets<Polyline>,
    materials: &mut Assets<PolylineMaterial>,
    custom_orbit_count: usize,
) {
    ui.collapsing("Create Orbit".to_string(), |ui| {
        let orbit_ref = &mut **new_orbit;

        orbit_grid(
            "new_orbit_settings_grid".to_string(),
            &mut orbit_ref.semi_major_axis,
            &mut orbit_ref.eccentricity,
            &mut orbit_ref.inclination,
            &mut orbit_ref.longitude_of_ascending_node,
            &mut orbit_ref.argument_of_periapsis,
            &mut orbit_ref.mean_anomaly,
            ui,
        );

        ui.vertical_centered_justified(|ui| {
            if ui.button("Create").clicked() {
                commands
                    .spawn(OrbitBundle {
                        orbit: **new_orbit,
                        polyline_bundle: PolylineBundle {
                            polyline: polylines.add(Polyline::default()),
                            material: materials.add(PolylineMaterial {
                                width: 2.0,
                                ..default()
                            }),
                            ..default()
                        },
                        ..default()
                    })
                    .insert(BodyName(format!("Orbit {}", custom_orbit_count + 1)));
            }
        });
    });
}

fn orbit_grid(
    id: String,
    semi_major_axis: &mut f64,
    eccentricity: &mut f64,
    inclination: &mut f64,
    longitude_of_ascending_node: &mut f64,
    argument_of_periapsis: &mut f64,
    mean_anomaly: &mut f64,
    ui: &mut Ui,
) {
    Grid::new(id)
        .num_columns(2)
        .spacing((10.0, 10.0))
        .striped(true)
        .show(ui, |ui| {
            ui.label("Semi-major axis");
            ui.add(DragValue::new(semi_major_axis).suffix("m"));
            ui.end_row();
            ui.label("Eccentricity");
            ui.add(
                DragValue::new(eccentricity)
                    .clamp_range(0.0..=1.0)
                    .speed(0.01),
            );
            ui.end_row();
            ui.label("Inclination");
            ui.add(angle_drag_value(inclination));
            ui.end_row();
            ui.label("Longitude of the ascending node");
            ui.add(angle_drag_value(longitude_of_ascending_node));
            ui.end_row();
            ui.label("Argument of periapsis");
            ui.add(angle_drag_value(argument_of_periapsis));
            ui.end_row();
            ui.label("Mean anomaly");
            ui.add(angle_drag_value(mean_anomaly));
            ui.end_row();
        });
}

fn angle_drag_value(value: &mut f64) -> DragValue {
    DragValue::new(value)
        .clamp_range(0.0..=TAU)
        .speed(0.008)
        .max_decimals(2)
        .suffix("rad")
}
