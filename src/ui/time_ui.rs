use bevy::prelude::*;
use bevy_egui::egui::{Align2, DragValue, Window as EguiWindow};
use bevy_egui::egui::{Grid, TextEdit};
use bevy_egui::EguiContexts;
use chrono::{DateTime, NaiveDateTime, Utc};

use crate::bodies::{Epoch, IngameTime, TimeState, Timescale};

pub struct TimeUiPlugin;

impl Plugin for TimeUiPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<TimeSettings>()
            .add_systems(Update, orbit_settings_ui);
    }
}

// TODO: Manage time correctly
#[derive(Deref, DerefMut, Resource)]
pub struct TimeSettings {
    pub date_time: String,
}

impl Default for TimeSettings {
    fn default() -> Self {
        Self {
            date_time: "2000-01-01 12:00:00".to_string(),
        }
    }
}

fn orbit_settings_ui(
    mut egui_context: EguiContexts,
    mut time_settings: ResMut<TimeSettings>,
    mut ingame_time: ResMut<IngameTime>,
    mut timescale: ResMut<Timescale>,
    mut time_state: ResMut<TimeState>,
    epoch: Res<Epoch>,
) {
    EguiWindow::new("Time Settings")
        .anchor(Align2::RIGHT_TOP, (-10.0, 10.0))
        .default_width(220.0)
        .resizable(false)
        .show(egui_context.ctx_mut(), |ui| {
            Grid::new("epoch_settings_grid")
                .num_columns(2)
                .spacing((10.0, 10.0))
                .show(ui, |ui| {
                    ui.label("Date and Time");
                    ui.add(TextEdit::singleline(&mut time_settings.date_time).char_limit(19));
                    ui.end_row();
                });

            ui.vertical_centered_justified(|ui| {
                let naive_date_time =
                    NaiveDateTime::parse_from_str(&time_settings.date_time, "%Y-%m-%d %H:%M:%S");

                ui.add_enabled_ui(naive_date_time.is_ok(), |ui| {
                    if ui.button("Set Date and Time").clicked() {
                        let date_time =
                            DateTime::from_naive_utc_and_offset(naive_date_time.unwrap(), Utc);

                        **ingame_time = (date_time - **epoch).num_seconds() as f64;
                        *time_state = TimeState::Paused;
                    }
                });
            });

            Grid::new("timescale_settings_grid")
                .num_columns(2)
                .spacing((10.0, 10.0))
                .show(ui, |ui| {
                    ui.label("Timescale");
                    ui.add(DragValue::new(&mut **timescale));
                    ui.end_row();
                });

            ui.vertical_centered_justified(|ui| {
                let text = match *time_state {
                    TimeState::Playing => "Pause",
                    TimeState::Paused => "Play",
                };

                if ui.button(text).clicked() {
                    time_state.toggle();
                }
            });
        });
}
