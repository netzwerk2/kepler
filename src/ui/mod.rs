use bevy::prelude::*;
use bevy_egui::egui::epaint::Shadow;
use bevy_egui::egui::Visuals;
use bevy_egui::{EguiContexts, EguiPlugin};

use crate::ui::orbit_ui::OrbitUiPlugin;
use crate::ui::time_ui::TimeUiPlugin;

mod orbit_ui;
mod time_ui;

pub struct UiPlugin;

impl Plugin for UiPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins((EguiPlugin, OrbitUiPlugin, TimeUiPlugin))
            .add_systems(Startup, setup);
    }
}

fn setup(mut egui_context: EguiContexts) {
    let visuals = Visuals {
        window_shadow: Shadow::default(),
        ..Visuals::dark()
    };

    egui_context.ctx_mut().set_visuals(visuals);
}
