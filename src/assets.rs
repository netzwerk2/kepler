use bevy::prelude::*;

pub struct AssetsPlugin;

impl Plugin for AssetsPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<BodyAssets>();
    }
}

#[derive(Resource)]
pub struct BodyAssets {
    pub circle_texture: Handle<Image>,
    pub billboard_size: f32,
}

impl FromWorld for BodyAssets {
    fn from_world(world: &mut World) -> Self {
        let asset_server = world.resource::<AssetServer>();
        let circle_texture = asset_server.load("images/circle.png");

        let billboard_size = 16.0;

        Self {
            circle_texture,
            billboard_size,
        }
    }
}
