use crate::solar_system::SolarSystemBody;
use bevy::input::mouse::{MouseMotion, MouseWheel};
use bevy::prelude::*;
use bevy_dolly::prelude::*;
use orbit_tessellation::bevy_polyline::prelude::Polyline;
use orbit_tessellation::{Orbit, TessellationCamera};

pub struct CameraControllerPlugin;

impl Plugin for CameraControllerPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<CameraSettings>()
            .add_systems(Startup, setup)
            .add_systems(
                Update,
                (
                    Dolly::<MainCamera>::update_active,
                    rotate_camera,
                    zoom_camera,
                ),
            )
            .add_systems(Last, auto_zoom_orbit);
    }
}

#[derive(Resource)]
pub struct CameraSettings {
    pub pan_sensitivity: f32,
    pub zoom_sensitivity: f32,
}

impl Default for CameraSettings {
    fn default() -> Self {
        Self {
            pan_sensitivity: 3.0,
            zoom_sensitivity: 1000.0,
        }
    }
}

#[derive(Component)]
pub struct MainCamera;

fn setup(mut commands: Commands) {
    commands
        .spawn((
            MainCamera,
            Rig::builder()
                .with(Position::new(Vec3::ZERO))
                .with(YawPitch::new().yaw_degrees(45.0).pitch_degrees(-30.0))
                .with(Smooth::new_position(0.3))
                .with(Smooth::new_rotation(0.3))
                .with(Arm::new(Vec3::Z * 1e12))
                .build(),
            Camera3dBundle::default(),
        ))
        .insert(TessellationCamera);
}

#[allow(clippy::too_many_arguments)]
fn rotate_camera(
    mut mouse_motion_events: EventReader<MouseMotion>,
    mouse_button_input: Res<Input<MouseButton>>,
    camera_settings: Res<CameraSettings>,
    mut rig_query: Query<&mut Rig, With<MainCamera>>,
) {
    let mut rig = rig_query.single_mut();
    let yaw_pitch_driver = rig.driver_mut::<YawPitch>();

    let delta = mouse_motion_events.read().map(|e| e.delta).sum::<Vec2>();

    if mouse_button_input.pressed(MouseButton::Right) {
        yaw_pitch_driver.rotate_yaw_pitch(
            -0.1 * delta.x * camera_settings.pan_sensitivity,
            -0.1 * delta.y * camera_settings.pan_sensitivity,
        );
    }
}

fn zoom_camera(
    mut mouse_wheel_events: EventReader<MouseWheel>,
    camera_settings: Res<CameraSettings>,
    mut rig_query: Query<(&mut Rig, &Transform), With<MainCamera>>,
) {
    let (mut rig, transform) = rig_query.single_mut();

    for mouse_wheel_event in mouse_wheel_events.read() {
        let arm = rig.driver_mut::<Arm>();
        arm.offset.z = (arm.offset.z
            - mouse_wheel_event.y
                * camera_settings.zoom_sensitivity
                * (transform.translation.length().powf(2.0 / 3.0) + 1.0))
            .abs();
    }
}

fn auto_zoom_orbit(
    polylines: Res<Assets<Polyline>>,
    orbit_query: Query<&Handle<Polyline>, (Added<Orbit>, Without<SolarSystemBody>)>,
    mut rig_query: Query<&mut Rig, With<MainCamera>>,
) {
    let mut rig = rig_query.single_mut();

    for handle in orbit_query.iter() {
        let max_distance = polylines
            .get(handle)
            .unwrap()
            .vertices
            .iter()
            .fold(0.0, |max_d, v| {
                if v.length() > max_d {
                    v.length()
                } else {
                    max_d
                }
            });

        rig.driver_mut::<Arm>().offset.z = max_distance * 3.0;
    }
}
