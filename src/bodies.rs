use bevy::prelude::*;
use chrono::{DateTime, NaiveDate, NaiveDateTime, NaiveTime, Utc};
use orbit_tessellation::Orbit;

use crate::assets::BodyAssets;
use crate::billboard::{Billboard, BillboardPlugin};
use crate::solar_system::SolarSystemBody;

pub struct BodyPlugin;

impl Plugin for BodyPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<Epoch>()
            .init_resource::<Timescale>()
            .init_resource::<IngameTime>()
            .init_resource::<TimeState>()
            .add_plugins(BillboardPlugin)
            .add_systems(PreUpdate, update_ingame_time)
            .add_systems(Update, spawn_body_mesh)
            .add_systems(Last, update_body_positions);
    }
}

#[derive(Deref, DerefMut, Resource)]
pub struct Epoch(pub DateTime<Utc>);

impl Default for Epoch {
    fn default() -> Self {
        Self(DateTime::from_naive_utc_and_offset(
            NaiveDateTime::new(
                NaiveDate::from_ymd_opt(2000, 1, 1).unwrap(),
                NaiveTime::from_hms_opt(12, 0, 0).unwrap(),
            ),
            Utc,
        ))
    }
}

#[derive(Deref, DerefMut, Resource)]
pub struct Timescale(pub f64);

impl Default for Timescale {
    fn default() -> Self {
        Self(5e6)
    }
}

#[derive(Default, Deref, DerefMut, Resource)]
pub struct IngameTime(pub f64);

#[derive(Default, Eq, PartialEq, Resource)]
pub enum TimeState {
    #[default]
    Playing,
    Paused,
}

impl TimeState {
    pub fn toggle(&mut self) {
        *self = match self {
            TimeState::Playing => TimeState::Paused,
            TimeState::Paused => TimeState::Playing,
        }
    }
}

#[derive(Component, Deref, DerefMut)]
pub struct BodyPosition(pub Vec3);

#[derive(Component, Deref, DerefMut)]
pub struct BodyName(pub String);

fn update_ingame_time(
    time: Res<Time>,
    time_state: Res<TimeState>,
    timescale: Res<Timescale>,
    mut ingame_time: ResMut<IngameTime>,
) {
    if *time_state == TimeState::Playing {
        **ingame_time += time.delta_seconds_f64() * **timescale;
    }
}

fn spawn_body_mesh(
    mut commands: Commands,
    body_assets: Res<BodyAssets>,
    orbit_query: Query<(Entity, Option<&SolarSystemBody>), Added<Orbit>>,
) {
    for (entity, body) in orbit_query.iter() {
        let color = match body {
            None => Color::WHITE,
            Some(b) => b.color(),
        };

        commands.entity(entity).insert(BodyPosition(Vec3::ZERO));
        commands
            .spawn(SpriteBundle {
                texture: body_assets.circle_texture.clone_weak(),
                sprite: Sprite {
                    color,
                    custom_size: Some(Vec2::splat(body_assets.billboard_size)),
                    ..default()
                },
                ..default()
            })
            .insert(Billboard(entity));
    }
}

fn update_body_positions(
    ingame_time: Res<IngameTime>,
    mut body_query: Query<(&mut BodyPosition, &Orbit)>,
) {
    for (mut body_position, orbit) in body_query.iter_mut() {
        **body_position = orbit.calculate_position(**ingame_time).as_vec3();
    }
}
