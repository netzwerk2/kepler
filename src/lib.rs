#![allow(clippy::too_many_arguments)]
#![allow(clippy::type_complexity)]

use bevy::prelude::*;
use orbit_tessellation::OrbitTessellationPlugin;

use crate::assets::AssetsPlugin;
use crate::bodies::BodyPlugin;
use crate::camera::CameraControllerPlugin;
use crate::solar_system::SolarSystemPlugin;
use crate::ui::UiPlugin;

mod assets;
mod billboard;
mod bodies;
mod camera;
mod solar_system;
mod ui;

pub const SUN_MU: f64 = 1.327124400189e20;

pub struct KeplerPlugin;

impl Plugin for KeplerPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(ClearColor(Color::BLACK))
            .insert_resource(AmbientLight {
                brightness: 0.0,
                ..default()
            })
            .add_plugins((
                OrbitTessellationPlugin,
                AssetsPlugin,
                CameraControllerPlugin,
                SolarSystemPlugin,
                BodyPlugin,
                UiPlugin,
            ));
    }
}
