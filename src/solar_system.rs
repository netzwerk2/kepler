use std::fmt::{Debug, Display, Formatter};
use std::fs;
use std::fs::File;
use std::io::BufWriter;

use bevy::prelude::*;
use bevy::utils::HashMap;
use bevy_tokio_tasks::{TokioTasksPlugin, TokioTasksRuntime};
use chrono::{DateTime, Duration, NaiveDateTime, Utc};
use orbit_tessellation::bevy_polyline::prelude::*;
use orbit_tessellation::{Orbit, OrbitBundle};
use rhorizons::EphemerisOrbitalElementsItem;
use ron::ser::PrettyConfig;
use strum::IntoEnumIterator;
use strum_macros::EnumIter;

use crate::bodies::{BodyName, Epoch};
use crate::SUN_MU;

const CACHE_FILE: &str = "orbits.ron";

pub struct SolarSystemPlugin;

impl Plugin for SolarSystemPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins(TokioTasksPlugin::default())
            .add_systems(Startup, spawn_solar_system)
            .add_systems(Update, cache_solar_system_bodies);
    }
}

#[derive(Component, Copy, Clone, Debug, EnumIter)]
pub enum SolarSystemBody {
    Mercury,
    Venus,
    Earth,
    Mars,
    Jupiter,
    Saturn,
    Uranus,
    Neptune,
    Pluto,
}

impl SolarSystemBody {
    pub fn id(&self) -> i32 {
        match self {
            SolarSystemBody::Mercury => 199,
            SolarSystemBody::Venus => 299,
            SolarSystemBody::Earth => 399,
            SolarSystemBody::Mars => 499,
            SolarSystemBody::Jupiter => 599,
            SolarSystemBody::Saturn => 699,
            SolarSystemBody::Uranus => 799,
            SolarSystemBody::Neptune => 899,
            SolarSystemBody::Pluto => 999,
        }
    }

    pub fn color(&self) -> Color {
        match self {
            SolarSystemBody::Mercury => Color::rgb(0.7, 0.7, 0.7),
            SolarSystemBody::Venus => Color::rgb(1.0, 0.3, 1.0),
            SolarSystemBody::Earth => Color::rgb(0.3, 0.3, 1.0),
            SolarSystemBody::Mars => Color::ORANGE,
            SolarSystemBody::Jupiter => Color::ORANGE_RED,
            SolarSystemBody::Saturn => Color::YELLOW,
            SolarSystemBody::Uranus => Color::ALICE_BLUE,
            SolarSystemBody::Neptune => Color::BLUE,
            SolarSystemBody::Pluto => Color::ANTIQUE_WHITE,
        }
    }

    pub fn iter_cache_status() -> Box<dyn Iterator<Item = (SolarSystemBody, CacheStatus)>> {
        if let Ok(file_contents) = fs::read_to_string(CACHE_FILE) {
            let Some(cached_orbits) = read_cached_orbits(&file_contents) else {
                return Box::new(Self::iter().map(|b| (b, CacheStatus::Uncached)));
            };

            let cached_orbits_map: HashMap<i32, Orbit> = HashMap::from_iter(cached_orbits);

            Box::new(Self::iter().map(move |b| {
                if let Some(orbit) = cached_orbits_map.get(&b.id()) {
                    (b, CacheStatus::Cached(*orbit))
                } else {
                    (b, CacheStatus::Uncached)
                }
            }))
        } else {
            Box::new(Self::iter().map(|b| (b, CacheStatus::Uncached)))
        }
    }
}

impl Display for SolarSystemBody {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

pub enum CacheStatus {
    Cached(Orbit),
    Uncached,
}

fn spawn_solar_system(tokio_runtime: ResMut<TokioTasksRuntime>, epoch: Res<Epoch>) {
    let start = **epoch;
    let stop = DateTime::from_naive_utc_and_offset(
        NaiveDateTime::new(start.date_naive() + Duration::days(1), start.time()),
        Utc,
    );

    for (body, status) in SolarSystemBody::iter_cache_status() {
        tokio_runtime.spawn_background_task(move |mut ctx| async move {
            let orbit = match status {
                CacheStatus::Cached(o) => o,
                CacheStatus::Uncached => {
                    let orbital_elements =
                        rhorizons::ephemeris_orbital_elements(body.id(), start, stop).await;
                    Orbit::from_rhorizons(&orbital_elements[0])
                }
            };

            ctx.run_on_main_thread(move |ctx| {
                let world = ctx.world;

                let mut polylines = world.resource_mut::<Assets<Polyline>>();
                let polyline = polylines.add(Polyline::default());

                let mut materials = world.resource_mut::<Assets<PolylineMaterial>>();
                let material = materials.add(PolylineMaterial {
                    color: body.color(),
                    width: 1.0,
                    ..default()
                });

                world
                    .spawn(OrbitBundle {
                        orbit,
                        polyline_bundle: PolylineBundle {
                            polyline,
                            material,
                            ..default()
                        },
                        ..default()
                    })
                    .insert((body, BodyName(body.to_string())));
            })
            .await;
        });
    }
}

fn cache_solar_system_bodies(
    body_query: Query<(&SolarSystemBody, &Orbit), Added<SolarSystemBody>>,
) {
    if body_query.is_empty() {
        return;
    }

    let mut orbits = vec![];

    if let Ok(file_contents) = fs::read_to_string(CACHE_FILE) {
        if let Some(cached_orbits) = read_cached_orbits(&file_contents) {
            orbits = cached_orbits;
        }
    }

    orbits.extend(body_query.iter().map(|(b, o)| (b.id(), *o)));

    let file = File::create(CACHE_FILE).unwrap();
    let writer = BufWriter::new(file);

    ron::ser::to_writer_pretty(writer, &orbits, PrettyConfig::default()).unwrap();
}

fn read_cached_orbits(file_contents: &str) -> Option<Vec<(i32, Orbit)>> {
    let cached_orbits = match ron::from_str(file_contents) {
        Ok(o) => o,
        Err(e) => {
            error!("failed to parse orbit cache: '{}'", e);
            return None;
        }
    };

    Some(cached_orbits)
}

trait OrbitExt {
    fn from_rhorizons(item: &EphemerisOrbitalElementsItem) -> Self;
}

impl OrbitExt for Orbit {
    fn from_rhorizons(item: &EphemerisOrbitalElementsItem) -> Self {
        Self {
            semi_major_axis: item.semi_major_axis as f64 * 1e3,
            eccentricity: item.eccentricity as f64,
            inclination: item.inclination.to_radians() as f64,
            longitude_of_ascending_node: item.longitude_of_ascending_node.to_radians() as f64,
            argument_of_periapsis: item.argument_of_perifocus.to_radians() as f64,
            mean_anomaly: item.mean_anomaly.to_radians() as f64,
            gravitational_parameter: SUN_MU,
            time: 0.0,
        }
    }
}
